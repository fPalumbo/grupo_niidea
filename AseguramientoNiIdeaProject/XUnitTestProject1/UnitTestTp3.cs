using System;
using Xunit;

namespace XUnitTestProject1
{
    public class UnitTestTp3
    {
        private CashRegister CashRegister;

        public UnitTestTp3()
        {
            CashRegister = new CashRegister(1000);
        }

        [Fact]
        public void Test001_ACashRegisterWhenReceivingMoneyShouldIncreaseMoneyInside()
        {
            CashRegister.AddMoney(500);

            Assert.Equal(1500, CashRegister.MoneyInside);
        }

        [Fact]
        public void Test002_ACashRegisterWhenReceivingNegativeMoneyShouldThrowArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => CashRegister.AddMoney(-50));
        }

        [Fact]
        public void Test003_ACashRegisterWhenReceivingNoMoneyShouldThrowArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => CashRegister.AddMoney(0));
        }

        [Fact]
        public void Test004_ACashRegisterWhenTakingMoneyShouldDecreaseMoneyInside()
        {
            CashRegister.TakeMoney(500);

            Assert.Equal(500, CashRegister.MoneyInside);
        }

        [Fact]
        public void Test005_ACashRegisterWhenTakingNegativeMoneyShouldThrowArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => CashRegister.TakeMoney(-50));
        }

        [Fact]
        public void Test006_ACashRegisterWhenTakingNoMoneyShouldThrowArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => CashRegister.TakeMoney(0));
        }

        [Fact]
        public void Test007_ACashRegisterWhenOpenedShouldBeOpen()
        {
            Assert.False(CashRegister.IsOpen);

            CashRegister.Open();

            Assert.True(CashRegister.IsOpen);
        }

        [Fact]
        public void Test008_ACashRegisterWhenClosedShouldNotBeOpen()
        {
            Assert.False(CashRegister.IsOpen);

            CashRegister.Open();

            Assert.True(CashRegister.IsOpen);

            CashRegister.Close();

            Assert.False(CashRegister.IsOpen);
        }

    }

    public class CashRegister
    {

        public double MoneyInside { get; private set; }

        public bool IsOpen { get; private set; }

        public void Open() { IsOpen = true; }

        public void Close() { IsOpen = false; }

        public CashRegister()
        {
            MoneyInside = 0;
            IsOpen = false;
        }

        public CashRegister(double moneyInside)
        {
            if (moneyInside < 0)
            {
                moneyInside = 0;
            }
            MoneyInside = moneyInside;
            IsOpen = false;
        }

        public void AddMoney(double moneyToAdd)
        {
            if (moneyToAdd <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            MoneyInside += moneyToAdd;
            return;
        }

        internal void TakeMoney(double moneyToTake)
        {
            if (moneyToTake <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            MoneyInside -= moneyToTake;
        }
    }
}